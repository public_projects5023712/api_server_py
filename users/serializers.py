from rest_framework import serializers
from .models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name']
        
    def create(self, validated_data):
        # Genera un nombre de usuario único basado en el nombre y el ID del usuario
        unique_username = validated_data['name'].lower().replace(' ', '') + str(User.objects.latest('id').id + 1)
        
        # Asigna el nombre de usuario único al diccionario de datos validados
        validated_data['username'] = unique_username

        # Crea el usuario utilizando los datos validados actualizados
        user = User.objects.create(**validated_data)
        return user
