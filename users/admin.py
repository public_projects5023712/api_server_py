from django.contrib import admin
from .models import User

# Registra el modelo User en el administrador
admin.site.register(User)