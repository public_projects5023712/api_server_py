from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer

class CreateUserAPIView(APIView):
    def post(self, request):
        try:
            serializer = UserSerializer(data=request.data)
            # Verificar si los datos son válidos
            if serializer.is_valid():
                # Si son válidos, guardar el usuario
                serializer.save()
                # Devolver una respuesta
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                # Si los datos no son válidos, devolver los errores de validación con un código de estado 400 Bad Request
                errors = serializer.errors
                return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            # Capturar cualquier excepción que ocurra durante el proceso y devolver un mensaje de error genérico con un código de estado 500 Internal Server Error
            error_message = "Error al crear el usuario."
            return Response({'error': error_message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)