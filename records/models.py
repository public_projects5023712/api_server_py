from djongo import models

class Record(models.Model):
    # Campos relacionados con MySQL
    user_id = models.IntegerField()  # Almacena el ID del usuario de MySQL
    product_id = models.IntegerField()  # Almacena el ID del producto de MySQL
    
    # Campos específicos de MongoDB
    serie = models.BigIntegerField()
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    activate_at = models.DateTimeField()
    status = models.BooleanField(default=False)

    class Meta:
        app_label = 'records'

    def get_user(self):
        # Método para obtener el usuario relacionado desde MySQL
        from users.models import User
        return User.objects.using('default').get(pk=self.user_id)

    def get_product(self):
        # Método para obtener el producto relacionado desde MySQL
        from products.models import Product
        return Product.objects.using('default').get(pk=self.product_id)
