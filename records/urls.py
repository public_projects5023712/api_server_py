from django.urls import path
from .views import CreateRecordAPIView

urlpatterns = [
    path('record', CreateRecordAPIView.as_view(), name='create_record'),
]
