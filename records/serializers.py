from rest_framework import serializers
from .models import Record
from users.models import User
from users.serializers import UserSerializer
from products.serializers import ProductSerializer 

class RecordSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    product = serializers.SerializerMethodField()

    class Meta:
        model = Record
        fields = '__all__'

    def get_user(self, obj):
        try:
            user = User.objects.using('default').get(pk=obj.user_id)
            return UserSerializer(user).data
        except User.DoesNotExist:
            return {"id": obj.user_id}

    def get_product(self, obj):
        # get_product para recuperar el producto asociado y serializarlo
        product = obj.get_product()
        return ProductSerializer(product).data    
