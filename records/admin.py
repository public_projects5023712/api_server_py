from django.contrib import admin
from .models import Record

# Registra el modelo Record en el administrador
admin.site.register(Record)
