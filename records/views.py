from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import RecordSerializer
from users.serializers import UserSerializer
from .models import Record


class CreateRecordAPIView(APIView):
    def post(self, request):
        try:
            # Crear el usuario utilizando el serializador de usuario
            user_serializer = UserSerializer(data=request.data)
            if user_serializer.is_valid():
                new_user = user_serializer.save()

                # Crear el registro con el ID del nuevo usuario
                request.data['user_id'] = new_user.id
                record_serializer = RecordSerializer(data=request.data)
                if record_serializer.is_valid():
                    record_serializer.save()
                    return Response(record_serializer.data, status=status.HTTP_201_CREATED)
                else:
                    # Eliminar el usuario recién creado si hay un error al crear el registro
                    new_user.delete()
                    return Response(record_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        
    def get(self, request):
        products = Record.objects.all()
        serializer = RecordSerializer(products, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    
    def put(self, request):
        try:
            record_id = request.query_params.get("id")
            if record_id is None:
                return Response({"error": "No se proporcionó 'id'."}, status=status.HTTP_400_BAD_REQUEST)

            # Intenta obtener el Record específico, maneja el caso de no encontrado
            try:
                record = Record.objects.get(pk=record_id)
            except Record.DoesNotExist:
                return Response({'error': 'Registro no encontrado.'}, status=status.HTTP_404_NOT_FOUND)

            # Actualiza el estado del registro a True
            record.status = True
            record.save()


            serializer = RecordSerializer(record)
            return Response(serializer.data, status=status.HTTP_200_OK)
        
        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)