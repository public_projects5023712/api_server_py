from django.contrib import admin
from .models import Product

# Registra el modelo Products en el administrador
admin.site.register(Product)
