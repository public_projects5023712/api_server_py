from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=255, null=False)
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 'products_products'
    
    def __str__(self):
        return self.name
